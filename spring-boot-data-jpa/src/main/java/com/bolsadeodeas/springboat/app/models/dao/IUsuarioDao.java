package com.bolsadeodeas.springboat.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeodeas.springboat.app.models.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{
	
	public Usuario findByUsername(String username);

}
